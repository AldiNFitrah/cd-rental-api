# CD Rental API

An API that enables rental owner to manage his rental. Owner can add/edit/delete CDs, and manage the customers' order.


## Installation

### 1. Getting all files

Unzip and extract all the files to a directory.


### 2. Environment Files

The zipped file comes with a .env.example in the root project. Make sure you create your own .env, and change the settings (especially the database) according to your environment.

Generate your APP_KEY. It's usually a random string with 32 characters.


### 3. Composer

The dependencies of this API managed by the php composer. So you need to install all the dependencies by navigating to the project directory in your terminal and type this command:

```bash
composer install
```


### 4. Database

To use the API, you need to migrate all tables that the app required to your database. You can simply migrate it by typing this command:

```bash
php artisan migrate --seed
```

It will migrate the tables and seed the tables with some initial data.


## Usage

I assume you already installed XAMPP (or MAMP, LAMP, etc), so what you have to do now is to start the MySQL service through the XAMPP Control Panel.

After that, we can serve the API now. Simply type this command in your terminal:

```bash
php -S localhost:8000 -t public
```

and we're ready to go.


### ~ Making Request to the API

Open Postman (or any application that has the same purposes), and we can start sending request and getting response from our API.

But before accessing all the features, first you need to register a user account, then login to get the API Token. This will be used to authorize you when accessing the features.

Add the token you obtained on the header when you are using postman, and now you can manage your own CD Rental.

You can read the full API documentation in [this link](https://drive.google.com/open?id=17RiFCww9SeZimn2PfQEEG05HLvXeZtfeeoFEdeD_OS0) 


### ~ Code Coverage

To run the unit test and see the code coverage, you can do it by this command:

```bash
vendor\bin\phpunit
```

If you have XDebug with you, it will generate the report and you can find it in /report/index.html.


## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).


## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.


## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)