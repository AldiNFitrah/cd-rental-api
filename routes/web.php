<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });

$router->group(['prefix' => 'admin'], function() use ($router) {

    $router->post('register', 'AuthController@register');
    
    $router->post('login', 'AuthController@login');
});

$router->group(['prefix' => 'category'], function() use ($router) {

    $router->get('', 'CategoryController@index');

    $router->post('', 'CategoryController@store');
    
    $router->put('{id}', 'CategoryController@update');

    $router->delete('{id}', 'CategoryController@destroy');
});

$router->group(['prefix' => 'cd'], function() use ($router) {

    $router->get('', 'CDController@index');

    $router->post('', 'CDController@store');

    $router->get('{pk}', 'CDController@show');

    $router->put('{pk}', 'CDController@update');

    $router->delete('{pk}', 'CDController@destroy');
});

$router->group(['prefix' => 'rent'], function() use ($router) {

    $router->get('', 'OrderController@index');
    
    $router->post('', 'OrderController@store');

    $router->get('{pk}', 'OrderController@show');
    
    $router->post('{pk}/return', 'OrderController@pay_back');

    $router->delete('{pk}', 'OrderController@destroy');
});
