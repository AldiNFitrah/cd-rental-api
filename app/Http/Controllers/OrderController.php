<?php

namespace App\Http\Controllers;
use Brick\Math\BigDecimal;
use Illuminate\Http\Request;
use App\Order;
use App\CD;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Return the total_price of an order.
     */
    private static function getTotalPrice($data)
    {
        if(isset($data->returned_date))
        {
            $borrowed = Carbon::parse($data->rented_date);
            $returned = Carbon::parse($data->returned_date);

            $rate = BigDecimal::of($data->rate_atm);
            $days = BigDecimal::of($returned->diffInDays($borrowed));
            $qty = BigDecimal::of($data->quantity);
            $total = $rate->multipliedBy($days)->multipliedBy($qty);

            return $total->__toString();
        }
        return null;
    }

    /**
     * Return all Orders.
     */
    public function index()
    {
        $datas = Order::all();
        
        foreach ($datas as $data)
        {
            $data->total_price = $this->getTotalPrice($data);
        }

        return response($datas, 200);
    }
    
    /**
     * Create an order.
     */
    public function store(Request $request)
    {
        $this->validate($request, ['cd_id' => 'required|exists:cds,id']);

        $data = new Order();
        $data->cd_id = $request->input('cd_id');
        $cd = CD::findOrFail($data->cd_id);

        $this->validate($request, [
            'quantity' => 'required|integer|min:0|max:'.$cd->quantity,
            'rented_date' => 'date_format:Y-m-d',
        ]);

        $data->rate_atm = $cd->rate;
        $data->quantity = $request->input('quantity');
        $data->rented_date = $request->input('rented_date', date('Y-m-d')); // Default is today's date.
        
        $cd->quantity -= $data->quantity;

        $data->save();
        $cd->save();

        return response(null, 201);
    }

    /**
     * Get details of an order.
     */
    public function show($id)
    {
        $data = Order::findOrFail($id);
        $data->total_price = $this->getTotalPrice($data);

        return response($data, 200);
    }

    /**
     * Finished a rental.
     */
    public function pay_back(Request $request, $id)
    {
        $data = Order::findOrFail($id);
        
        $this->validate($request, [
            'returned_date' => 'date_format:Y-m-d|after_or_equal:'.$data->rented_date,
        ]);

        $initial_returned_date = $data->returned_date;

        $data->returned_date = $request->input('returned_date', date('Y-m-d')); // Default is today's date.

        if(empty($initial_returned_date))
        {
            $cd = CD::findOrFail($data->cd_id);
            $cd->quantity += $data->quantity;
            
            $cd->save();
        }

        $data->save();

        return response(null, 204);
    }

    /**
     * Delete an order.
     */
    public function destroy($id)    
    {
        $data = Order::findOrFail($id);

        if(empty($data->returned_date))
        {
            $cd = CD::findOrFail($data->cd_id);
            $cd->quantity += $data->quantity;

            $cd->save();
        }
        $data->delete();

        return response(null, 204);
    }
}
