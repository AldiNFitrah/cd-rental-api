<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    /**
     * Register a user.
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users,username',
            'password' => 'required',
        ]);

        $user = new User();
        $user->username = $request->input('username');
        $user->password = Hash::make($request->input('password'));

        $user->save();

        return response(null, 201);
    }

    /**
     * Authenticate user and return the API Token.
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::where('username', $request->input('username'))->firstOrFail();

        if(Hash::check($request->input('password'), $user->password)){
            $user->api_token = base64_encode(str_random(64));
            $user->save();

            return response()->json(["api_token" => $user->api_token], 200);

        } else {

            return response(null, 401);
        }   
    }
}
