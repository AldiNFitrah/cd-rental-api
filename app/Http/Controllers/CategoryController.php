<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Return all categories.
     */
    public function index()
    {
        $datas = Category::all();

        return response($datas, 200);
    }

    /**
     * Create new category.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:categories,name',
        ]);

        $data = new Category();
        $data->name = $request->input('name');
        $data->save();

        return response(null, 201);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'max:255|unique:categories,name',
        ]);

        $data = Category::findOrFail($id);
        $data->name = $request->input('name', $data->name);
        $data->save();

        return response(null, 204);
    }

    /**
     * Delete a category.
     */
    public function destroy($id)
    {
        $data = Category::findOrFail($id);
        $data->delete();
            
        return response(null, 204);
    }
}
