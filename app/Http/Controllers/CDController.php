<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\CD;

class CDController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Return all CDs.
     */
    public function index()
    {
        $datas = CD::all();
        return response($datas, 200);
    }
    
    /**
     * Create new CD.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'category_id' => 'exists:categories,id',
            'rate' => 'required|numeric|min:0',
            'quantity' => 'required|integer|min:0',
        ]);

        $data = new CD();
        $data->title = $request->input('title');
        $data->category_id = $request->input('category_id', 1); // Set category to Others if none provided
        $data->rate = $request->input('rate');
        $data->quantity = $request->input('quantity');
        $data->save();
        
        return response(null, 201);
    }

    /**
     * Get CD instance details.
     */
    public function show($id)
    {
        $data = CD::findOrFail($id);
        return response($data, 200);
    }

    /**
     * Change CD information(s).
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'max:255',
            'category_id' => 'exists:categories,id',
            'rate' => 'numeric|min:0',
            'quantity' => 'integer|min:0',
        ]);
        
        $data = CD::findOrFail($id);
        $data->title = $request->input('title', $data->title);
        $data->category_id = $request->input('category_id', $data->category_id);
        $data->rate = $request->input('rate', $data->rate);
        $data->quantity = $request->input('quantity', $data->quantity);
        $data->save();
    
        return response(null, 204);
    }
    
    /**
     * Delete a CD.
     */
    public function destroy($id)
    {
        $data = CD::findOrFail($id);
        $data->delete();
        
        return response(null, 204);
    }
}
