<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CD extends Model
{
    protected $table = 'cds';

    protected $fillable = [
        'title',
        'category_id',
        'rate',
        'quantity',
    ];

    public function categories()
    {
        return $this->belongsTo('App\Category');
    }

    public function borrowings()
    {
        return $this->hasMany('App\Order');
    }
}
