<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model 
{
    protected $table = 'orders';

    protected $fillable = [
        'cd_id',
        'quantity',
        'rented_date',
        'returned_date',
    ];

    protected $guarded = [
        'rate_atm',
    ];
    public function cds()
    {
        return $this->belongsTo('App\CD');
    }
}
