<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use App\Order;

class OrderTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Seeding creates a category named 'Others'.
     * Also create a fake CD and rental.
     */
    public function setUp()
    {
        parent::setUp();

        $this->artisan('db:seed');

        $this->post('cd', [
            'title' => 'first_record',
            'category_id' => 1,
            'rate' => 2000,
            'quantity' => 5,
        ]);

        $this->post('rent', [
           'cd_id' => 1,
           'quantity' => 3,
           'rented_date' => '2020-03-19'
        ]);
    }

    /**
     * URL availability.
     */
    public function testURL()
    {
        $this->get('rent');
        $this->seeStatusCode(200);
    }

    /**
     * New order creation.
     */
    public function testCreateOrder()
    {
        $this->post('rent', [
            'cd_id' => '1',
            'quantity' => 2,
            'rented_date' => '2020-03-19'
        ]);

        $this->assertCount(2, Order::all());
    }

    /**
     * Return the CDs and finish the rental.
     */
    public function testFinishRental()
    {
        $this->post('rent/1/return', ['returned_date' => '2020-03-20']);
        $this->assertNotNull(Order::where('id', 1)->first()->returned_date);
    }

    /**
     * Getting order details with the total price calculated.
     */
    public function testGetOrderDetail()
    {
        $this->post('rent/1', ['returned_date' => '2020-03-20']);
        $this->get('rent/1')->seeJsonStructure(['total_price']);
    }

    /**
     * CD Deletion.
     */
    public function testDeleteOrder()
    {
        $this->delete('rent/1');
        $this->assertEmpty(Order::all());
    }
}
