<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use App\CD;

class CDTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Seeding creates a category named 'Others'.
     * Also create a fake CD.
     */
    public function setUp()
    {
        parent::setUp();

        $this->artisan('db:seed');

        $this->post('cd', [
            'title' => 'first_record',
            'category_id' => 1,
            'rate' => 2000,
            'quantity' => 5,
        ]);
    }

    /**
     * URL availability.
     */
    public function testURL()
    {
        $this->get('cd');
        $this->seeStatusCode(200);
    }

    /**
     * New CD creation.
     */
    public function testCreateCD()
    {
        $this->post('cd', [
            'title' => 'second_record',
            'rate' => 7000,
            'quantity' => 10,
        ]);
        $this->seeInDatabase('cds', ['title' => 'second_record']);
    }

    /**
     * Getting CD Details.
     */
    public function testGetCD()
    {
        $this->get('cd/1');
        $this->seeJsonContains(['title' => 'first_record']);
    }

    /**
     * Change an information(s) of a CD.
     */
    public function testUpdateCD()
    {
        $this->put('cd/1', ['title' => 'another_record']);
        $this->seeInDatabase('cds', ['title' => 'another_record']);
    }

    /**
     * CD Deletion.
     */
    public function testDeleteCD()
    {
        $this->delete('cd/1');
        $this->assertEmpty(CD::all());
    }
}
