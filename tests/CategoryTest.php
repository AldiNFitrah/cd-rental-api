<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use App\Category;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;
    
    /**
     * Seeding creates a category named 'Others'.
     */
    public function setUp()
    {
        parent::setUp();

        $this->artisan('db:seed');
    }

    /**
     * URL avaibility.
     */
    public function testURL()
    {
        $this->get('category');

        $this->seeStatusCode(200);
    }

    /**
     * New category creation.
     */
    public function testCreateCategory()
    {
        $this->post('category', ['name' => 'new_category']);
        $this->seeInDatabase('categories', ['name' => 'new_category']);
    }

    /**
     * Category deletion.
     */
    public function testDeleteCategory()
    {
        $this->delete('category/4');
        $this->seeStatusCode(404);

        $this->delete('category/1');
        $this->assertCount(0, Category::all());
    }
}
